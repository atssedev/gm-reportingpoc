﻿using System.Diagnostics;
using System.Linq;
using DevExpress.DataAccess.ConnectionParameters;
using DevExpress.DataAccess.EntityFramework;
using DevExpress.DataAccess.Sql;
using DevExpress.XtraReports.UI;
using Microsoft.AspNetCore.Mvc;
using ReportingPOC.Models;
using ReportingPOC.Models.Db;

namespace ReportingPOC.Controllers {
    public class HomeController : Controller {
        ReportContext dbContext;
        public HomeController(ReportContext context) {
            dbContext = context;
        }
        public IActionResult Index() {
            var reports = dbContext.Reports.ToList()?.Select(c=>new ReportModel { Id = c.Id, Name = c.Name, Description = c.Description});
            return View(reports);
        }


        public IActionResult ReportDesigner(int? id) {
            ViewData["Message"] = "DevExpress Reporting. Report Designer Control.";
           
            if(id!=null)
                return View(new ReportDesignerModel() { ReportUrl = id.ToString() });
            else
            {
                return View(new ReportDesignerModel() { Report = new XtraReport() });
            }
            
        }

        public IActionResult ReportViewer(int id) {
            ViewData["Message"] = "DevExpress Reporting. Web Document Viewer Control.";
            var model = new WebDocumentViewerModel {
                ReportUrl = id.ToString()
            };
            return View(model);
        }

        public IActionResult Error() {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
