﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ReportingPOC.Models.Db
{
    [Table("report_data_source")]
    public partial class ReportDataSource
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("report_id")]
        public int ReportId { get; set; }
        [Column("data_source_id")]
        public int DataSourceId { get; set; }
    }
}
