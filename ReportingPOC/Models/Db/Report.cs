﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ReportingPOC.Models.Db
{
    [Table("report")]
    public partial class Report
    {
        [Column("report_id")]
        [Key]
        public int Id { get; set; }
        [Column("report_name")]
        public string Name { get; set; }
        [Column("report_description")]
        public string Description { get; set; }
        [Column("report_layout_xml")]
        public byte[] LayoutXml { get; set; }
    }
}
