﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ReportingPOC.Models.Db
{
    [Table("data_source_parameters")]
    public partial class DataSourceParameters
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("data_source_id")]
        public int DataSourceId { get; set; }
        [Column("parameter_name")]
        public string Name { get; set; }
        [Column("parameter_description")]
        public string Description { get; set; }
        [Column("required")]
        public bool? Required { get; set; }
        [Column("parameter_type")]
        public int? Type { get; set; }
        [Column("parameter_sql")]
        public string SQL { get; set; }
        [Column("lkp_key_field")]
        public string LkpKeyField { get; set; }
        [Column("lkp_display_field")]
        public string LkpDisplayField { get; set; }
        [Column("lkp_is_sql")]
        public bool? LkpIsSql { get; set; }
    }
}