﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ReportingPOC.Models.Db
{
    [Table("data_source")]
    public partial class DataSource
    {
        [Key]
        [Column("data_source_id")]
        public int Id { get; set; }
        [Column("data_source_name")]
        public string Name { get; set; }
        [Column("query")]
        public string Query { get; set; }
        [Column("is_sp")]
        public bool? IsSP { get; set; }
        [Column("disabled")]
        public bool? IsDisabled { get; set; }
    }
}
