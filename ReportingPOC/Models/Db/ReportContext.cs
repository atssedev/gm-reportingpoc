﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportingPOC.Models.Db
{
    public class ReportContext : DbContext
    {

        public ReportContext(DbContextOptions<ReportContext> options) : base(options)
        {
        }
        
        public virtual DbSet<Report> Reports { get; set; }
        public virtual DbSet<DataSource> DataSources { get; set; }
        public virtual DbSet<ReportDataSource> ReportDataSources { get; set; }
        public virtual DbSet<DataSourceParameters> DataSourceParameters { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("Reporting");
        }
    }
}
