﻿using DevExpress.DataAccess.Sql;
using DevExpress.DataAccess.Web;

namespace ReportingPOC.Services
{
    public class DataSourceWizardDBSchemaProviderFactory : IDataSourceWizardDBSchemaProviderExFactory
    {
        public IDBSchemaProviderEx Create()
        {
            return new DBSchemaProvider();
        }
    }
}