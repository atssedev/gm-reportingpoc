﻿using DevExpress.DataAccess.Sql;
using DevExpress.Xpo.DB;
using System.Linq;

namespace ReportingPOC.Services
{
    public class DBSchemaProvider : IDBSchemaProviderEx
    {
        DBSchemaProviderEx provider;
        public DBSchemaProvider()
        {
            this.provider = new DBSchemaProviderEx();
        }

        public DBTable[] GetTables(SqlDataConnection connection, params string[] tableList)
        {
            return provider.GetTables(connection, tableList)
                .Where(table => table.Name.StartsWith("Reporting."))
                .ToArray();
        }

        public DBTable[] GetViews(SqlDataConnection connection, params string[] viewList)
        {
            return provider.GetViews(connection, viewList)
                .Where(view => view.Name.StartsWith("Reporting."))
                .ToArray();
        }

        public DBStoredProcedure[] GetProcedures(SqlDataConnection connection, params string[] procedureList)
        {
            return provider.GetProcedures(connection, procedureList).Where(storedProcedure => storedProcedure.Name != null && storedProcedure.Name.StartsWith("Reporting.")).ToArray(); ;
        }

        public void LoadColumns(SqlDataConnection connection, params DBTable[] tables)
        {
            provider.LoadColumns(connection, tables);
        }
    }
}