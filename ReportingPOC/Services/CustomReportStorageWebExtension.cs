﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Web.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using DevExpress.XtraReports.UI;
using ReportingPOC.Models.Db;

namespace ReportingPOC.Services
{
    
    public class DBReportStorageWebExtension : ReportStorageWebExtension
    {
        private ReportContext dbContext;
        
        public DBReportStorageWebExtension(ReportContext context)
        {
            dbContext = context;
        }


        public override bool CanSetData(string url)
        {
            // Check if the URL is available in the report storage.
            return dbContext.Reports.Find(int.Parse(url)) != null;
        }


        public override byte[] GetData(string url)
        {
            // Get the report data from the storage.
            var report = dbContext.Reports.Find(int.Parse(url));
            if (report == null) return null;
            if (report.LayoutXml == null)
            {
                var rpt = new XtraReport();
                using (MemoryStream ms = new MemoryStream())
                {
                    rpt.SaveLayoutToXml(ms);
                    return ms.GetBuffer();
                }
            }
            return report.LayoutXml;
        }


        public override Dictionary<string, string> GetUrls()
        {
            // Get URLs and display names for all reports available in the storage.
            return dbContext.Reports.ToList()
                  .ToDictionary(c => c.Id.ToString(), c => c.Name);
        }


        public override bool IsValidUrl(string url)
        {
            // Check if the specified URL is valid for the current report storage.
            // In this example, a URL should be a string containing a numeric value that is used as a data row primary key.
            int n;
            return int.TryParse(url, out n);
        }


        public override void SetData(XtraReport report, string url)
        {
            // Write a report to the storage under the specified URL.
            var reportDb = dbContext.Reports.Find(int.Parse(url));

            if (reportDb != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    report.SaveLayoutToXml(ms);
                    reportDb.LayoutXml = ms.GetBuffer();
                    dbContext.SaveChanges();
                }
                
            }
        }


        public override string SetNewData(XtraReport report, string defaultUrl)
        {
            // Save a report to the storage under a new URL. 
            // The defaultUrl parameter contains the report display name specified by a user.
            Report reportDb = new Report
            {
                Name = defaultUrl
            };
            using (MemoryStream ms = new MemoryStream())
            {
                report.SaveLayoutToXml(ms);
                reportDb.LayoutXml = ms.GetBuffer();
            }

            dbContext.Reports.Add(reportDb);
            var id = dbContext.SaveChanges();

            return reportDb.Id.ToString();
        }
    }
}

