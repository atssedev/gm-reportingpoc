﻿using System;
using System.IO;
using DevExpress.AspNetCore;
using DevExpress.AspNetCore.Reporting;
using DevExpress.Utils;
using DevExpress.XtraReports.Security;
using DevExpress.XtraReports.Web.Extensions;
using DevExpress.XtraReports.Web.ReportDesigner;
using DevExpress.XtraReports.Web.WebDocumentViewer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ReportingPOC.Models.Db;
using ReportingPOC.Services;

namespace ReportingPOC {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.AddMvc().AddDefaultReportingControllers();
            services.AddDevExpressControls();

            //services.ConfigureReportingServices(configurator => {
            //    configurator.ConfigureReportDesigner(designerConfigurator => {
            //        designerConfigurator.RegisterDataSourceWizardConfigFileConnectionStringsProvider();
            //    });
            //});

            
            services.ConfigureReportingServices(configurator => {
                configurator.ConfigureQueryBuilder(c => { c.RegisterDataSourceWizardDBSchemaProviderExFactory<DataSourceWizardDBSchemaProviderFactory>(); });
                configurator.ConfigureReportDesigner(designerConfigurator => {
                    designerConfigurator.RegisterDataSourceWizardConnectionStringsProvider<DataSourceWizardConnectionStringsProvider>(false);
                });
            });
            
            services
               .AddLogging()
               .AddDbContext<ReportContext>(options =>
        options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddEntityFrameworkSqlServer();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            System.AppDomain.CurrentDomain.SetData("DataDirectory", env.ContentRootPath);
            if(env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            } else {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes => {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseDevExpressControls();
            var optionsBuilder = new DbContextOptionsBuilder<ReportContext>();
            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            ReportStorageWebExtension.RegisterExtensionGlobal(new DBReportStorageWebExtension(new ReportContext(optionsBuilder.Options)));

            ScriptPermissionManager.GlobalInstance = new ScriptPermissionManager(ExecutionMode.Deny);
            UrlAccessSecurityLevelSetting.SafeSetSecurityLevel(UrlAccessSecurityLevel.WebUrlsOnly);
        }
    }
}
